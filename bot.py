from ConfigParser import RawConfigParser as ConfParser
from optparse import OptionParser
import ConfigParser
import codecs
from select import select
from irc import *
import socket
import sys
import os

my = IRCbot()
config = ConfigParser.RawConfigParser()
config.readfp(codecs.open("config.ini", "r", "utf8"))
irc = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
irc.connect ((config.get('connection', 'server'),config.getint('connection', 'port')))
irc.send('NICK %s\r\n' % config.get('core' ,'nickname'))
irc.send('USER %s %s %s :%s\r\n' % (config.get('core' ,'ident'), config.get('core', 'ident'), config.get('connection', 'bindhost'), config.get('core', 'realname')))
irc.send('JOIN %s,%s,%s\r\n' % (config.get('channels', 'invitechan'), config.get('channels' ,'inviteonly'), config.get('channels', 'logchan')))
irc.send('PRIVMSG NickServ :identify %s\r\n' % config.get('services', 'nickpass'))
irc.send('AWAY :%s\r\n' % config.get('core', 'away'))
while 1:
    (rl, wl, xl) = select([irc], [], []) #IRC only for now.
    for sock in rl:
        data = readline(sock)
        raw = parse(data)
        if sock == irc:
            print data
            if raw[0].lower() == "ping":
               irc.send("PONG {reply}\r\n".format(reply = raw[1]))
